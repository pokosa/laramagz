package main

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"github.com/gorilla/mux"
	"io"
	"log"
	"net/http"
	"time"
)

const baseUrl = "https://a.klaviyo.com/api/"

type pubSub struct {
	Message message `json:"message"`
}

type message struct {
	//Attributes map[string]string `json:"attributes"`
	Data string `json:"data"`
}

func (m *message) decode() {
	decodeString, err := base64.StdEncoding.DecodeString(m.Data)
	if err != nil {
		return
	}
	m.Data = string(decodeString)
}

type data struct {
	Token              string            `json:"token"`
	Event              string            `json:"event"`
	CustomerProperties map[string]string `json:"customer_properties"`
	Properties         map[string]string `json:"properties"`
}

func trackProfileActivity(w http.ResponseWriter, r *http.Request) {
	url := baseUrl + "track"
	bodyByte, _ := io.ReadAll(r.Body)
	log.Println("Request body: ", string(bodyByte))
	p := pubSub{}
	_ = json.Unmarshal(bodyByte, &p)
	p.Message.decode()
	log.Println(p.Message)
	d := data{}
	_ = json.Unmarshal([]byte(p.Message.Data), &d)
	dJson, _ := json.Marshal(d)
	log.Println(string(dJson))
	r, _ = http.NewRequest(http.MethodPost, url, bytes.NewBuffer(dJson))
	r.Header.Set("Content-Type", "application/json")
	client := &http.Client{Timeout: time.Second * 5}
	resp, err := client.Do(r)
	if err != nil {
		log.Fatal(err)
	}
	defer r.Body.Close()
	if resp.StatusCode == http.StatusOK {
		//bodyBytes, err := io.ReadAll(resp.Body)
		if err != nil {
			log.Fatal(err)
		}
		//bodyString := string(bodyBytes)
		//log.Println(bodyString)
		//log.Println(resp.StatusCode)
	}

}
func main() {
	r := mux.NewRouter()
	r.HandleFunc("/trackProfileActivity", trackProfileActivity).Methods(http.MethodPost).Headers("Content-Type", "application/json")
	server := &http.Server{
		Handler: r,
		Addr:    ":8080",
		// Good practice: enforce timeouts for servers you create!
		WriteTimeout: 5 * time.Second,
		ReadTimeout:  5 * time.Second,
	}
	log.Fatal(server.ListenAndServe())

}
